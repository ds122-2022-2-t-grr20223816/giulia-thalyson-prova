<?php
include 'credentials.php';

$conn = mysqli_connect($host, $user, $pass, $db);
if(!$conn){
  die('Problemas ao conectar ao BD: ' . mysqli_connect_error());
}

$sql = "SELECT Name, Price, ImageURL FROM Products";
$result = $conn->query($sql);

?>
<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <meta charset="UTF-8">
    <link href="style.css" rel="stylesheet">
  </head>
  <body>
    <h1>Produtos</h1>
    <ul>
    <form action="<?= $_SERVER['PHP_SELF']?>" method="post">
    <?php
    while($row = $result->fetch_assoc()) {
      echo "Produto: " .$row["Name"]. " " . $row["Price"]. "                ";
      echo "<input type='submit' name=bot value= 'adicionar ao carrinho'>";
      echo "<br> <br> <br>";
      echo '<img src="'.$row['ImageURL'].'" width=200 height=200>';
      echo "<br> <br> <br>";
    }
    ?>
    </form>
    </ul>
    <h1>Carrinho</h1>

    <p>Total: </p>
  </body>
</html>
