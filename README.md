Tecnologia em Análise e Desenvolvimento de Sistemas

Setor de Educação Profissional e Tecnológica - SEPT

Universidade Federal do Paraná - UFPR

---

*DS122 - Desenvolvimento Web 1*

Prof. Alexander Robert Kutzke

# Exercício Prova

Produza uma aplicação com o seguinte comportamento:

A aplicação simula um carrinho de compras, com opções de adição e remoção de produtos do carrinho.

A aplicação deve possuir apenas uma página (`cart.php`). Nesta página, devem ser exibidos todos os produtos presentes na
tabela `Products` juntamente com um ou mais botões para adição de produtos ao carrinho (produtos repetidos no carrinho são permitidos).

Na mesma tela, o conteúdo do carrinho deve ser exibido, com opção de remoção de cada item.

Além disso, a aplicação deve exibir o valor total da compra atual que consta no carrinho. Os preços são salvos em formato `INT`, sempre armazenando o valor do produto em centavos. Porém a exibição aos usuários deve ser feita em reais e centavos.

Cada item do carrinho possui uma quantidade. O manuseio dessa quantidade é livre, mas deve estar presente (pode ser determinado no momento da adição do produto ao carrinho, pode ser escolhida após o produto estar no carrinho ou qualquer outra forma).

A aplicação deve conter um belo estilo, no arquivo `style.css`;

## Detalhes técnicos

O arquivo `credentials.php` já está configurado para conexão com um BD remoto. Apenas adicione o seu à variável  `$db`. Por exemplo:

```php
<?php
  $host = "200.236.3.126";
  $user = "root";
  $pass = "example";
  $db = "GRR12341234"; // COLOQUE SEU GRR AQUI
?>
```

Utilize as letras `GRR` maiúsculas.

O BD já possui tabelas e itens pré adicionados. Os comandos utilizados foram os seguintes:

```sql
CREATE TABLE Products (
    Id int NOT NULL AUTO_INCREMENT,
    Name varchar(255) NOT NULL,
    Price int,
    ImageURL varchar(512) NOT NULL,
    PRIMARY KEY (Id)
);

CREATE TABLE Cart (
    Id int NOT NULL AUTO_INCREMENT,
    ProductId int NOT NULL,
    Quantity int NOT NULL,
    PRIMARY KEY (Id),
    FOREIGN KEY (ProductId) REFERENCES Products(Id)
);

INSERT INTO Products (Name, Price, ImageURL) VALUES ('Fonte Corsair CX650F RGB', 54480, 'http://200.236.3.126:8999/images/fonte.jpg');
INSERT INTO Products (Name, Price, ImageURL) VALUES ('Switch Gigabit De Mesa Com 8 Portas', 16931, 'http://200.236.3.126:8999/images/switch.jpg');
INSERT INTO Products (Name, Price, ImageURL) VALUES ('Mouse sem fio Logitech M190', 4799, 'http://200.236.3.126:8999/images/mouse.jpg');
INSERT INTO Products (Name, Price, ImageURL) VALUES ('Adaptador de Rede Conversor USB 3.0 para RJ45', 4990, 'http://200.236.3.126:8999/images/adaptador.jpg');
INSERT INTO Products (Name, Price, ImageURL) VALUES ('Cabo de Fibra Óptica para Audio', 2765, 'http://200.236.3.126:8999/images/cabo_audio.jpg');
INSERT INTO Products (Name, Price, ImageURL) VALUES ('Webcam USB Full HD 1080P', 19999, 'http://200.236.3.126:8999/images/webcam.jpg');
INSERT INTO Products (Name, Price, ImageURL) VALUES ('Pendrive Multilaser Nano 8gb', 2681, 'http://200.236.3.126:8999/images/pendrive.jpg');
INSERT INTO Products (Name, Price, ImageURL) VALUES ('HDD Externo Seagate 1TB Expansion', 27900, 'http://200.236.3.126:8999/images/hd.jpg');

INSERT INTO Cart (ProductId, Quantity) VALUES (1, 2);
INSERT INTO Cart (ProductId, Quantity) VALUES (2, 3);

```
